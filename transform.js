'use strict';

const fs = require('fs');
const vulnerabilities = require('./gl-container-scanning-report.json')

let transformed = [];

function transformSeverity(scanningSeverity) {
    switch(scanningSeverity) {
        case "Low":
            return "minor";
        case "Medium":
            return "major";
        case "High": 
            return "critical";
        case "Critical":
            return "blocker"
        case "Unknown":
            return "unknown";
    }
}

vulnerabilities.vulnerabilities.forEach(element => {
    console.log(element);
    transformed.push({
        description: element.message,
        severity: transformSeverity(element.severity),
        fingerprint: element.id,
        location: {
            path: element.description
        }
    })
})


let result = JSON.stringify(transformed);
fs.writeFileSync('quality.json', result);
